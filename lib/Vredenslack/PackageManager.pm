package Vredenslack::PackageManager;

use v5.12;
use warnings;
use strict;

use Carp;
use Cwd;
use Data::Dumper;
use Digest::MD5 qw/md5_hex/;
use File::Basename;
use File::Copy;
use File::Find;
use File::Path qw/make_path/;
use Storable qw/nstore retrieve/;
use Term::ANSIColor ':constants';

use Vredenslack::PackageManager::Package;
use Vredenslack::Term::Indent qw/indent unindent indentation/;

#use EerieSoft::RecThis qw/rec_this rec_this_dump/;

our $VERSION = '0.4.3';
our $DBVERSION = '0.4';
our $INTERACTIVE_MODE = 0;

################################################################################ internal static methods

################################################################################ private methods

################################################################################ public static methods

#===============================================================================
sub interactive {
#===============================================================================
	my $i = shift;

	$INTERACTIVE_MODE = $i if defined $i;

	$INTERACTIVE_MODE;
}

################################################################################ public methods

#===============================================================================
sub new {
#===============================================================================
	my $class = shift;
	my $config = shift;

	my $data = {
		cache => {},
		cfg  => $config,
		dbfn => $config->database_file
	};

	# read the architecture
	($data->{arch} = `uname -m`) =~ s/\n|\r//;
	if ($data->{arch} =~ /^i.86$/) {
		$data->{arch} = 'i486';
	} elsif ($data->{arch} =~ /^arm.*$/) {
		$data->{arch} = 'arm';
	}

	my $self = bless $data, $class;

	# load the package database or sync and build package database
	if (-e $self->{dbfn}) {
		$self->{db} = retrieve($self->{dbfn});
	} else {
		$self->scan();
		#nstore $self->{db}, $self->{cfg}->{'database-location'} or croak 'failed to save database, make sure you have write permissions for [', $self->{cfg}->{'database-location'}, ']';
	}

	$self;
}

#===============================================================================
sub scan {
#===============================================================================
	my $self   = shift;

	my @repos = $self->{cfg}->repos();

	print indentation, 'scanning repositories', RESET, "\n";
	indent;

	foreach my $rn (@repos) {
		my $repo = $self->{cfg}->repo($rn);
		my $repo_priority = $repo->priority();

		# auto update new repositories
		$repo->update() unless -d $repo->folder;

		print indentation, GREEN, 'scanning: ', RESET, $repo->folder, "\n";

		# go through the directory list and fetch package info
		my $package_data = {};
		find( sub {
			if (/\.info$/) {
				#
				open(my $fh, '<', $_);
				my $pkg = {};
				my $folder = $File::Find::dir; #) =~ s/$cwd//); $folder =~ s!^/!!g; # remove cwd and starting /
				$pkg->{FOLDER} = $folder;
				while(<$fh>) {
					if (/=/) {
						s/\n|\r//g;
						my ($a, $b) = split /=/, $_, 2;
						# multiline
						while ($b !~ /"\s*$/) {
							my $_tline = <$fh>;
							last unless defined $_tline;
							$_tline =~ s/\n|\r//g;
							$_tline =~ s/^\s+//;
							$b .= $_tline;
						}
						$b =~ s/"//g; # clean up "
						if ($b =~ m! \\!) {
							@{$pkg->{$a}} = split m!\\!, $b;
							map { s/(^\s+)|(\s+$)//g; } @{$pkg->{$a}};
						} else {
							$pkg->{$a} = $b unless ($b eq '' or $b eq 'UNSUPPORTED');
						}
					}
				}
				close($fh);

				# validate package
				for my $k (qw/PRGNAM VERSION/) {
					unless (defined $pkg->{$k}) {
						print RED, "Invalid info file ", $File::Find::name, ": no $k found", RESET, "\n" if $INTERACTIVE_MODE;
						return;
					}
				}
				unless (defined $pkg->{DOWNLOAD} or $pkg->{DOWNLOAD_x86_64}) {
					print RED, "Invalid info file ", $File::Find::name, ": no DOWNLOAD or DOWNLOAD_x86_64 found", RESET, "\n" if $INTERACTIVE_MODE;
					return;
				}

				# generate some extra fields
				if (defined $pkg->{DOWNLOAD}) {
					if (ref $pkg->{DOWNLOAD} eq 'ARRAY') {
						@{$pkg->{DOWNLOAD_FILE}} = map { (my $a = $_) =~ s!.*/([^/]+)!$1!; $a; } @{$pkg->{DOWNLOAD}};
					} else {
						($pkg->{DOWNLOAD_FILE} = $pkg->{DOWNLOAD}) =~ s!.*/([^/]+)!$1!;
					}
				}
				if (defined $pkg->{DOWNLOAD_x86_64}) {
					if (ref $pkg->{DOWNLOAD_x86_64} eq 'ARRAY') {
						@{$pkg->{DOWNLOAD_x86_64_FILE}} = map { (my $a = $_) =~ s!.*/([^/]+)!$1!; $a; } @{$pkg->{DOWNLOAD_x86_64}};
					} else {
						($pkg->{DOWNLOAD_x86_64_FILE} = $pkg->{DOWNLOAD_x86_64}) =~ s!.*/([^/]+)!$1!;
					}
				}

				# TODO:
				if (defined $self->{db}->{$pkg->{PRGNAM}}) {
					my $opkg = $self->{db}->{$pkg->{PRGNAM}};

					if (defined $opkg->{repo}) {
						my $or = $self->{cfg}->repo($opkg->{repo});
						my $orp = $or->priority();

						# save the package in the database if repo has same/higher priority
						if ($repo_priority >= $orp) {
							$pkg->{repo} = $rn;
							$self->{db}->{$pkg->{PRGNAM}} = $pkg;
						}
					} else {
						# save the package in the database
						$pkg->{repo} = $rn;
						$self->{db}->{$pkg->{PRGNAM}} = $pkg;
					}
				} else {
					# save the package in the database
					$self->{db}->{$pkg->{PRGNAM}} = $pkg;
				}

			}
		}, $repo->folder);
	}

	$self->save();
}

#===============================================================================
sub save {
#===============================================================================
	my $self = shift;

	nstore $self->{db}, $self->{dbfn} or croak 'failed to save database, make sure you have write permissions for [', $self->{dbfn}, ']';

	#rec_this 30, 'database saved';
}

#===============================================================================
sub list {
#===============================================================================
	my $self = shift;
	my $search = shift;

	return grep /$search/i, keys %{$self->{db}} if (defined $search);

	return keys %{$self->{db}};
}

#===============================================================================
sub get_package {
#===============================================================================
	my $self = shift;
	my $pn = shift;

	#rec_this 50, 'get_package(', $pn, ')';

	croak 'no package ', $pn, ' found' unless $self->{db}->{$pn};

	# check the cache
	if ($self->{cache}->{$pn}) {
		#rec_this 50, 'cache hit: ', $pn;
		return $self->{cache}->{$pn};
	}

	# create new package and return it
	$self->{cache}->{$pn} = Vredenslack::PackageManager::Package->new($self->{db}->{$pn}, $self->{arch});
	#rec_this 50, 'cache miss: ', $pn;

	return $self->{cache}->{$pn};
}

#===============================================================================
sub do {
#===============================================================================
	my $self = shift;
	my $pn   = shift;

	if ($self->{cfg}->is_ignored($pn)) {
		#rec_this 50, 'package is on the ignore list';
		print indentation, YELLOW, $self->{cfg}->ignore_reason($pn), RESET, "\n" if $INTERACTIVE_MODE;
		return 1;
	}

	#rec_this 50, 'preparing package ', $pn;
	my $pkg = $self->get_package($pn);

	my ($d, $b, $i) = (0, 0, 0);

	foreach (@_) {
		if ($_ eq 'download') {
			$d = 1;
		} elsif ($_ eq 'build') {
			$b = 1;
		} elsif ($_ eq 'install') {
			$i = 1;
		} else {
			croak 'bad action'
		}
	}

	if ($i and $pkg->is_installed() and not $pkg->has_update()) {
		print indentation, GREEN, 'already installed and no updates', RESET, ' [', BLUE, $pkg->name, RESET, ']', "\n";
		return 1;
	}

	if ($d) {
		print indentation, GREEN, 'downloading', RESET, ' [', BLUE, $pkg->name, RESET, ']', "\n";
		indent;
		unless ($pkg->download) {
			print indentation, RED, 'failed to download: ', $pkg->errstr, RESET, "\n";
			unindent;
			return 0;
		}
		unindent;

	}
	if ($b) {
		print indentation, GREEN, 'building', RESET, ' [', BLUE, $pkg->name, RESET, ']', "\n";
		indent;
		unless ($pkg->build) {
			print indentation, RED, 'failed to build: ', $pkg->errstr, RESET, "\n";
			unindent;
			return 0;
		}
		unindent;

	}
	if ($i) {
		print indentation, GREEN, 'installing', RESET, ' [', BLUE, $pkg->name, RESET, ']', "\n";
		indent;
		unless ($pkg->install) {
			print indentation, RED, 'failed to install: ', $pkg->errstr, RESET, "\n";
			unindent;

			return 0;
		}
		unindent;
	}

	return 1;
}

#===============================================================================
sub do_dependencies {
#===============================================================================
	my $self = shift;
	my $pn   = shift;

	if ($self->{cfg}->is_ignored($pn)) {
		#rec_this 50, 'package is on the ignore list';
		print indentation, YELLOW, $self->{cfg}->ignore_reason($pn), RESET, "\n" if $INTERACTIVE_MODE;
		return 1;
	}

	#rec_this 50, 'create package ', $pn;
	my $pkg = $self->get_package($pn);

	my @deps = $pkg->dependencies();
	my %dep_actions;

	foreach my $action (@_) {
		if ($action eq 'download') {
			$dep_actions{'download'} = 1;
		} elsif ($action eq 'build' or $action eq 'install') {
			$dep_actions{'build'}   = 1;
			$dep_actions{'install'} = 1;
		} else {
			croak 'bad action [', $action, ']';
		}
	}

	if (@deps) {
		my @text;
		push @text, $_ . 'ing' foreach keys %dep_actions;
		#rec_this 50, $action, 'ing package ', $pn, ' dependencies';
		print indentation, (join ', ', @text), ' [', GREEN, $pkg->name, RESET, '] dependencies: [', BLUE, (join ', ', @deps), RESET, ']', "\n";
		indent;

		foreach my $dep (@deps) {
			$self->do_dependencies($dep, keys %dep_actions) or return 0;
			$self->do($dep, keys %dep_actions) or return 0;
		}

		unindent;
		#rec_this 50, 'finished ', $action, 'ing ', $pn, ' dependencies';
	}

	return 1;
}

#===============================================================================
sub exclude {
#===============================================================================
	my $self = shift;
	my $ns = shift;
	my $pn = shift;

	$self->{cfg}->{excludes}->{$ns . '/' . $pn} = '';

	$self->_save_config;
}

#===============================================================================
sub include {
#===============================================================================
	my $self = shift;
	my $ns = shift;
	my $pn = shift;

	delete $self->{cfg}->{excludes}->{$ns . '/' . $pn};

	$self->_save_config;
}

#===============================================================================
sub exclude_list {
#===============================================================================
	my $self = shift;

	return keys %{$self->{cfg}->{excludes}};
}

1;

__END__

=head1 NAME

Vredenslack - a swiss knife for the typical Slackware/Linux user

=head1 SYNOPSIS


=head1 METHODS

=head2 list

Returns an array of all package names. If a scalar is passed as argument then only packages matching that string will be returned.

=head2 info (package name)

Returns full details of the package.

=head2 exclude (package namespace, package name)

Excludes a package from being resynched. It does not exclude it from being rescanned. Useful for making local changed on SlackBuilds
but still allow to use a normal update

=head2 include (package namespace, package name)

Remove a previously excluded package.

=head2 exclude_list

Returns a list of all packages excluded in the form of 'namespace'/'package name';

=head1 SEE ALSO

The SlackBuilds website at L<http://slackbuilds.org>

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
