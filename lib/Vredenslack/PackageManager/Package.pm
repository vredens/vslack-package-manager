package Vredenslack::PackageManager::Package;

#use EerieSoft::RecThis qw/rec_this rec_this_dump/;

use Carp;
use Cwd;
use Data::Dumper;
use Digest::MD5 qw/md5_hex/;
use File::Copy;
use File::Spec;

#===============================================================================
sub new {
#===============================================================================
	my $class = shift;
	my $data = shift;
	my $arch = shift;

	my $self = {};

	#rec_this_dump 50, $data;

	croak 'data has no PRGNAME' unless defined $data->{PRGNAM};
	croak 'data has no FOLDER'  unless defined $data->{FOLDER};

	# set some variables
	$self->{arch} = $arch;
	$self->{folder}  = $data->{FOLDER};
	$self->{version} = $data->{VERSION};
	$self->{name}    = $data->{PRGNAM};
	$self->{repo}    = $data->{repo};
	$self->{errstr}  = undef;

	# fetch build version
	$self->{build_version} = _get_build_version($self->{name}, $self->{folder});

	# check if there are any version installed
	$self->{installed_versions} = _get_installed_versions($self->{name}, $arch);

	# set the files which should be download for this machine
	my @download;
	my @md5sums;
	if ($arch eq 'x86_64' and defined $data->{DOWNLOAD_x86_64} and ! grep { $_ eq $data->{DOWNLOAD_x86_64} } qw/UNTESTED/) {
		if (ref $data->{DOWNLOAD_x86_64} eq 'ARRAY') {
			@download = @{$data->{DOWNLOAD_x86_64}};
			@md5sums = @{$data->{MD5SUM_x86_64}};
		} else {
			@download = ($data->{DOWNLOAD_x86_64});
			@md5sums = ($data->{MD5SUM_x86_64});
		}
	} elsif (defined $data->{DOWNLOAD}) {
		if (ref $data->{DOWNLOAD} eq 'ARRAY') {
			@download = @{$data->{DOWNLOAD}};
			@md5sums = @{$data->{MD5SUM}};
		} else {
			@download = ($data->{DOWNLOAD});
			@md5sums = ($data->{MD5SUM});
		}
	} else {
		croak 'no valid download option when creating package';
	}
	my $i = 0;
	$self->{download} = [];
	foreach (@download) {
		(my $fn = $_) =~ s!.*/([^/]+)!$1!;
		push @{$self->{download}}, [$_, $fn];
	}
	$self->{md5sums} = \@md5sums;

	# the readme file contents
	open(my $fh, '<', $self->{folder} . '/README') or die 'could not open ', $self->{folder}, '/README';
	my @l = <$fh>;
	close($fh);
	$self->{readme} = join '', @l;

	# split dependencies
	if ($data->{REQUIRES} ne '') {
		@{$self->{dependencies}} = grep { $_ =~ /^[-a-zA-Z0-9_]+$/ } split / /, $data->{REQUIRES};
	} else {
		$self->{dependencies} = [];
	}

	#rec_this 50, 'new package created: ', $self->{name};

	bless $self, $class;
}

#===============================================================================
sub name {
#===============================================================================
	my $self = shift;

	return $self->{name};
}

#===============================================================================
sub repository {
#===============================================================================
	my $self = shift;

	return $self->{repo};
}

#===============================================================================
sub build_location {
#===============================================================================
	my $self = shift;

	return $self->{folder};
}

#===============================================================================
sub is_installed {
#===============================================================================
	my $self = shift;

	return defined $self->{installed_versions}->[0];
}

#===============================================================================
sub installed_version {
#===============================================================================
	my $self = shift;

	return 0 unless $self->is_installed;

	return $self->{installed_versions}->[0]->[1];
}

#===============================================================================
sub has_update {
#===============================================================================
	my $self = shift;

	return 0 unless $self->is_installed;

	# compare versions
	return 1 if $self->{installed_versions}->[0]->[1] ne $self->{version};

	# compare build versions
	return $self->{installed_versions}->[0]->[2] < $self->{build_version};
}

#===============================================================================
sub dependencies {
#===============================================================================
	my $self = shift;

	return @{$self->{dependencies}};
}

#===============================================================================
sub filename_validator {
#===============================================================================
	my $self = shift;
	my $folder = shift;

	my $arch = $self->{arch};
	$arch = 'i.86' if $arch =~ /i.86/;

	my $prefix = quotemeta($self->{name} . '-' . $self->{version});
	my $sufix = quotemeta($self->{build_version} . '_SBo');

	return "$prefix.*-($arch|noarch)([-_].*)*-$sufix\.t(g|x)z";
}

#===============================================================================
sub download {
#===============================================================================
	my $self = shift;

	my $owd = getcwd;
	chdir $self->{folder} or return $self->_set_errstr('could not change folder to ', $self->{folder});

	#rec_this 50, 'current folder: ', getcwd;

	my @downloads = @{$self->{download}};

	#rec_this 50, 'downloading: ', (join ', ', map { $_->[1] } @downloads);

	foreach my $dl (@downloads) {
		my $uri = $dl->[0];
		my $fn   = $dl->[1];
		my $md5  = $dl->[2];

		# remove bad download
		if (-e $fn and ! _validate_md5($fn, @{$self->{md5sums}})) {
			#rec_this 20, 'found downloaded file ', $fn, ' but md5 checksum is invalid, removing...';
			unlink $fn or return $self->_set_errstr('could not remove file ', $fn);
			#rec_this 20, 'removed bad file ', $fn;
		}

		# check if file exists
		if (-e $fn and _validate_md5($fn, @{$self->{md5sums}})) {
			#rec_this 20, 'file already downloaded: ', $fn;
		} else {
			# download file
			#rec_this 50, 'downloading ', $fn, ' from ', $uri;
			`wget -q --no-check-certificate $uri` ;
			return $self->_set_errstr('could not download ', $fn) if ($!);
			return $self->_set_errstr('failed to download ', $fn) unless -e $fn;
			return $self->_set_errstr('bad md5sum of ', $fn, '.') unless _validate_md5($fn, @{$self->{md5sums}});
			#rec_this 50, $fn, ' download complete';
		}
	}

	chdir $owd;
}

#===============================================================================
sub _find_package {
#===============================================================================
	my $self = shift;
	my $folder = shift;

	my $arch = $self->{arch};
	$arch = 'i.86' if $arch =~ /i.86/;

	my $prefix = quotemeta($self->{name} . '-' . $self->{version});
	my $sufix = quotemeta($self->{build_version} . '_SBo');

	opendir(my $dh, $folder);
	while(readdir($dh)) {
		my $file = File::Spec->catfile($folder, $_);
		if (-f $file) {
			return $file if (/^$prefix.*-($arch|noarch)([-_].*)*-$sufix\.t(g|x)z$/);
		}
	}
	closedir($dh);

	undef;
}

#===============================================================================
sub build {
#===============================================================================
	my $self = shift;

	my $owd = getcwd;

	#rec_this 50, 'changing to ', $self->{folder}, ' from ', $owd;
	chdir $self->{folder} or return $self->_set_errstr('could not change folder to ', $self->{folder});

	my $sfile = $self->{name} . '.SlackBuild';
	my $bfile;;

	unless ($bfile = $self->_find_package('./')) {
		unless ($bfile = $self->_find_package("/tmp/")) {
			#rec_this 50, 'building the package';
			return $self->_set_errstr('SlackBuild script file not found [', getcwd, '/', $sfile, ']') unless -f $sfile;
			#rec_this 50, 'running build script. check log in ', getcwd, '/build.log';
			`sh $sfile > build.log 2>&1`;
			$? == 0 or return $self->_set_errstr('error running the build script. check ', getcwd, '/build.log for more info.');
			return $self->_set_errstr('package file not found in /tmp/') unless $bfile = $self->_find_package("/tmp/");
		}
		#rec_this 50, 'moving the package to ', getcwd;
		move ($bfile, './') or return $self->_set_errstr('could not move the build file into ', getcwd);
	}

	chdir $owd;
}

#===============================================================================
sub install {
#===============================================================================
	my $self = shift;

	my $owd = getcwd;
	chdir $self->{folder} or return $self->_set_errstr('could not change folder to ', $self->{folder});

	my $bfile;

	# die if we don't have a package to install
	$bfile = $self->_find_package('./') or return $self->_set_errstr('could not find the package file: ', $bfile);

	# remove any previously installed versions if they do not match the current version
	if ($self->is_installed) {
		unless ($self->has_update) {
			chdir $owd;
			return 1;
		}

		my @ivs = @{$self->{installed_versions}};

		foreach (@ivs) {
			my $p = $_->[0];
			`removepkg $p`;
			$? == 0 or return $self->_set_errstr('error removing package ', $self->{name}, ': ', $p);
		}
	}

	# install the package
	`installpkg $bfile`;
	$? == 0 or return $self->_set_errstr('failed to install package ', $bfile);

	# mark package as installed
	$self->{installed_versions} = _get_installed_versions($self->{name}, $arch);

	#rec_this 50, 'installed ', $self->{name};

	chdir $owd;

	return 1;
}

#===============================================================================
sub errstr {
#===============================================================================
	my $self = shift;

	$self->{errstr};
}

#===============================================================================
sub _set_errstr {
#===============================================================================
	my $self = shift;

	$self->{errstr} = join '', @_;

	return 0;
}

#===============================================================================
sub _get_build_version {
#===============================================================================
	my $name = shift;
	my $folder = shift;

	croak 'invalid folder [', $folder, ']' unless -d $folder;

	# find out the build version
	my $file = $folder . '/' . $name . '.SlackBuild';
	my $bv = 1;

	open (my $fh, '<', $file) or croak 'failed to open ', $file;
	while (<$fh>) {
		if (/^BUILD=.*(\d+)/) {
			$bv = $1;
			last;
		}
	}
	close ($fh);

	return $bv;
}

#===============================================================================
sub _get_installed_versions {
#===============================================================================
	my $name = shift;
	my $arch = shift;

	my $pkg_name = $name . '-';

	my @installed;

	opendir (my $dh, '/var/log/packages') or die 'failed to open /var/log/packages folder';
	my @packages = readdir($dh);
	foreach (@packages) {
		#/^$prefix.*-($arch|noarch)([-_].*)*-$sufix$/
		if (/^$pkg_name([0-9a-z\.]+)-($arch|noarch)([-_].*)*-([0-9])_SBo$/) {
			push @installed, [$_, $1, $^N];
		}
	}

	return \@installed;
}

#===============================================================================
sub _get_package_name {
#===============================================================================
	my $name = shift;
	my $version = shift;
	my $arch = shift;

	return $name . '-' . $version . '-' . $arch;
}

#===============================================================================
sub _validate_md5 {
#===============================================================================
	my $file = shift;
	my @md5  = @_;

	local $/ = undef;
	open (my $_fh, '<', $file);
	binmode $_fh;
	my $data = <$_fh>;
	close ($_fh);

	my $_md5 = md5_hex($data);

	for (@md5) {
		return 1 if $_ eq $_md5
	}

	return 0;
}


1;

__END__

=head1 NAME

Vredenslack::PackageManager::Package - Package representation

=head1 SYNOPSIS

=head1 DESCRIPTION

All 'public' methods return either 0 or 1. They return 0 if an error occurred
and return 1 if the method executed properly. You can then invoke the errstr
method which returns an error string that better describes the error.

Note that errstr only returns the last error message.

=head1 METHODS

=head2 new

Creates a new package

=head2 name

Returns the package name

=head2 is_installed

Returns true if the package is installed, false otherwise.

=head2 has_update

Returns true if the package is installed but there is an update available.

=head2 dependencies

Returns the list of dependencies

=head2 download

Downloads the package sources

=head2 build

Builds the package

=head2 install

Installs the package and might remove any previously installed version.

=head2 errstr

Returns the error message of the last error.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
