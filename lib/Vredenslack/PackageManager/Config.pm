package Vredenslack::PackageManager::Config;

use v5.12;
use warnings;
use strict;

use Carp;
use Cwd qw/realpath/;
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Find;
use File::Path qw/make_path/;
use File::Spec;
use Storable qw/dclone nstore retrieve/;

use Vredenslack::PackageManager;
use Vredenslack::PackageManager::Repository;

my $SLACKWARE_VERSION;
my $SLACKWARE_STABLE_VERSION = '14.1';
my $SLACKWARE_CURRENT_VERSION = '14.2';

#===============================================================================
sub _get_slack_version() {
#===============================================================================
	return $SLACKWARE_VERSION if $SLACKWARE_VERSION;

	my $version;

	croak 'not a Slackware dist' unless -f '/etc/slackware-version';

	open (my $_fh, '<', '/etc/slackware-version');
	my $v = readline $_fh;
	close ($_fh);

	if ($v =~ /(\d+\.\d+)/) {
		$version = $1;
	}

	croak "Slackware version $version is not supported" unless (defined $version and grep /^$version$/, ('13.37', '14.0', '14.1', $SLACKWARE_CURRENT_VERSION));

	$SLACKWARE_VERSION = $version;
	return $version;
}

#===============================================================================
sub new {
#===============================================================================
	my $class = shift;
	my $path = shift;
	my $opts = shift;

	my $self = {
		'path' => $path,
		'slack-version' => $opts->{version} || _get_slack_version(),
	};

	# fetch absolute path for the configuration file
	my $_cfg_folder = dirname($self->{path});
	make_path($_cfg_folder) unless -d $_cfg_folder;
	$self->{path} = realpath($self->{path});

	# create a new configuration or load an existing one
	if ($opts->{init} or not -e $self->{'path'}) {
		# in the case of the base folder not existing we create it first (this is before obtaining the absolute path)
		if (defined $opts->{'base-folder'} and not -d $opts->{'base-folder'}) {
			make_path($opts->{'base-folder'}) or croak 'failed to create base folder [', $opts->{'base-folder'}, ']';
		}

		# create the repository folder if one is missing
		my $_folder = realpath($opts->{'base-folder'}) || '/var/lib/vredenslack/pm';
		make_path($_folder) or croak 'failed to create base-folder [', $_folder, ']' unless -d $_folder;

		my $cfg = {
			'version'      => $Vredenslack::PackageManager::VERSION,
			'repo-folder'  => File::Spec->catfile($_folder, 'repos'),
			'base-folder'  => $_folder,
			'ignore'       => {},
			'repositories' => {}
		};

		# add repositories for most current slackware versions
		for my $v (qw/13.37 14.0 14.1/) {
			my $name = 'sbo-' . $v;
			my $url  = "rsync://slackbuilds.org/slackbuilds/$v";
			my $repo = new Vredenslack::PackageManager::Repository($name, $url, $cfg->{'repo-folder'}, $v);
			$cfg->{repositories}->{$name} = $repo;
		}

		# add special repository for the slackware current
		$cfg->{repositories}->{'sbo-' . $SLACKWARE_CURRENT_VERSION} = new Vredenslack::PackageManager::Repository(
			'sbo-' . $SLACKWARE_CURRENT_VERSION,
			'rsync://slackbuilds.org/slackbuilds/' . $SLACKWARE_STABLE_VERSION,
			$cfg->{'repo-folder'},
			$SLACKWARE_CURRENT_VERSION);

		# store the initial configuration
		$self->{cfg} = $cfg;
		save($self);
	} else {
		$self->{cfg} = retrieve $self->{'path'} or croak 'could not read from the configuration file [', $self->{'path'}, ']. Make sure you have read permissions and it is the correct file. If the problem persists you should remove the file and let Vredenslack create a fresh one.';
		croak 'Configuration file [', $self->{path}, '] too old, please remove it and let Vredenslack\'s Package Manager recreate it for you.' unless $self->{cfg}->{version};

		# control self fixing
		my $fixed = 0;

		# auto upgrade configuration
		if ($self->{cfg}->{version} ne $Vredenslack::PackageManager::VERSION) {
			$self->{cfg}->{version} = $Vredenslack::PackageManager::VERSION;

			$fixed = 1;
		}

		# auto fix repositories
		for my $rname (keys %{$self->{cfg}->{repositories}}) {
			$fixed = 1 if $self->{cfg}->{repositories}->{$rname}->fix();
		}

		# auto-save if there was an auto-fix
		save($self) if $fixed;
	}

	bless $self, $class;
}

#===============================================================================
sub save {
#===============================================================================
	my $self = shift;

	nstore ($self->{cfg}, $self->{'path'}) or die 'could not save configuration file [', $self->{'path'}, ']. Make sure you have write permissions.';
}

#===============================================================================
sub database_file {
#===============================================================================
	my $self = shift;

	File::Spec->catfile($self->{cfg}->{'base-folder'}, 'vspm-v' . $Vredenslack::PackageManager::DBVERSION . '-sv' . $self->{'slack-version'} . '.db');
}

#===============================================================================
sub repo_add {
#===============================================================================
	my $self = shift;

	my ($name, $url, $version, $priority) = @_;

	$self->{cfg}->{repositories}->{$name} = new Vredenslack::PackageManager::Repository($name, $url, $self->{cfg}->{'repo-folder'}, $version, $priority);

	$self->save();
}

#===============================================================================
sub repo_priority {
#===============================================================================
	my $self = shift;

	my ($name, $priority) = @_;

	croak 'No repository name provided' unless defined $name;
	croak 'No priority specified' unless defined $priority;
	croak 'No such repository [', $name, ']' unless defined $self->{cfg}->{repositories}->{$name};

	$self->{cfg}->{repositories}->{$name}->priority($priority);

	$self->save();
}

#===============================================================================
sub repo_del {
#===============================================================================
	my $self = shift;
	my $name = shift;

	croak 'no repository by that name' unless defined $self->{cfg}->{repositories}->{$name};

	delete $self->{cfg}->{repositories}->{$name};

	$self->save();
}

#===============================================================================
sub repos {
#===============================================================================
	my $self = shift;

	my $src = $self->{cfg}->{repositories};

	my @repos;
	foreach my $k (keys %$src) {
		push @repos, $k if $src->{$k}->{version} eq 'all' or $src->{$k}->{version} eq $self->{'slack-version'};
	}

	@repos;
}

#===============================================================================
sub repo {
#===============================================================================
	my $self = shift;
	my $k = shift;

	croak 'repo ', $k, ' does not exist' unless $self->{cfg}->{repositories}->{$k};

	my $repo = $self->{cfg}->{repositories}->{$k};

	# if repo has been
	$self->save() if $repo->fix();

	$repo;
}

#===============================================================================
sub ignore {
#===============================================================================
	my $self = shift;
	my $pn = shift;
	my $msg = shift || '';

	$msg = $pn . ' is ignored: ' . $msg;

	#rec_this 50, 'adding ', $pn, ' to the ignore list';
	$self->{cfg}->{ignore}->{$pn} = $msg;

	$self->save();
}

#===============================================================================
sub is_ignored {
#===============================================================================
	my $self = shift;
	my $pn = shift;

	defined $self->{cfg}->{ignore}->{$pn};
}

#===============================================================================
sub ignore_reason {
#===============================================================================
	my $self = shift;
	my $pn = shift;

	croak 'package is not ignored' unless $self->{cfg}->{ignore}->{$pn};

	$self->{cfg}->{ignore}->{$pn};
}

#===============================================================================
sub unignore {
#===============================================================================
	my $self = shift;
	my $pn = shift;

	croak 'no package name provided' unless $pn;

	#rec_this 50, 'removing ', $pn, ' from the ignore list';
	delete $self->{cfg}->{ignore}->{$pn};

	$self->save();
}

#===============================================================================
sub ignore_list {
#===============================================================================
	my $self = shift;

	return keys %{$self->{cfg}->{ignore}};
}


1;

__END__

=head1 NAME

Vredenslack::PackageManager::Config

=head1 SYNOPSIS

=head1 DESCRIPTION

All 'public' methods return either 0 or 1. They return 0 if an error occurred
and return 1 if the method executed properly. You can then invoke the errstr
method which returns an error string that better describes the error.

Note that errstr only returns the last error message.

=head1 METHODS

=head2 new($path)

Creates a new configuration at $path. If $path does not exist then a new
configuration file is created with default values, otherwise the file is read
and checked for a valid configuration.

=head2 database_file()

Returns the package list database file location. This method is used by the PackageManager.

=head2 ignore($package_name, $message)

Ignores the given package.

=head2 ignore_list()

=head2 ignore_reason($package_name)

=head2 is_ignored($package_name)

Returns true if the $package_name is in the ignore list.

=head2 unignore($package_name)

Removes a package from the ignore list.

=head2 repo($repository_name)

Returns the Repository instance for the given repository name.

=head2 repo_add($repository_name, $repository_url, $slackware_version)

Adds a new repository.

=head2 repo_del($repository_name)

Removes a repository.

=head2 repos()

Returns all repositories for the current slackware version.

=head2 save()

Saves the configuration.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
