# NAME

vs-pm - VSlack Package Manager

# ABOUT

VSlack is a collection of tools for Slackware administration and typical
usage. It is aimed at developers, power users and administrators who
typically reinstall, upgrade, test stuff.

VSlack Package Manager is somewhat similar to sbopkg [http://www.sbopkg.org/](http://www.sbopkg.org/)
with a bit less functionality but tries to go a bit further, providing:

- Automatic dependency installation based on the dependency list of packages.
- Ignore packages. Useful to ignore packages which you have already installed in some other form.
- Lets you control what to do with the package and its dependencies.
- Support for multiple repositories so you can add your own or other online repositories which contain SlackBuilds format. Supports adding repositories which are an rsync server, a remote git repository or a local folder!
- Install packages from another slackware version. Useful when there is a new slackware version out and some packages haven't been upgraded yet.

# INSTALLATION

    git clone http://bitbucket.org/eeriesoftronics/vslack-package-manager.git
    cd vslack-package-manager
    cpanm .

# SYNOPSIS

## CONFIGURATION

    vs-pm config init --config-file /etc/vredenslack/pm/config --base-folder /var/lib/vredenslack/pm
    vs-pm [OPTIONS] config show

## PACKAGE MANAGEMENT

    vs-pm [OPTIONS] update
    vs-pm [OPTIONS] rescan

    vs-pm [OPTIONS] list [<search term>]
    vs-pm [OPTIONS] info <package name>

    vs-pm [OPTIONS] install <package name> [<package name> ...]
    vs-pm [OPTIONS] uninstall <package name> [<package name> ...]

    vs-pm [OPTIONS] ignore <package name> [<reason>]
    vs-pm [OPTIONS] unignore <package name>

## REPOSITORIES

    vs-pm [OPTIONS] repo add <name> <address> <version> [<priority>]
    vs-pm [OPTIONS] repo remove <name>
    vs-pm [OPTIONS] repo priority <name> <priority>
    vs-pm [OPTIONS] repo list

# OPTIONS

## General Options

    --config-file <path>   use a configuration file other than the default one
    --slackware-version    force a certain slackware version
    --quiet                only outputs errors
    --help                 show a more detailed help

## Configuration Options

    --base-folder <folder>  specify a different base folder for your repositories

## Package Management

    --noinstall    do not install packages
    --nobuild      do not build packages
    --nodownload   do not download packages
    --only-deps    only do actions on packages' dependencies
    --nodeps       do not follow up dependencies for packages you are installing

# CONFIGURATION

Vredenslack Package Manager (_VS-PM_ for short) requires a configuration file and a folder with enough storage space to store replicas of the slackbuilds repositories you want to add as well as all packages you build. The recomended storage for a power user would be 1-2 GB. I myself have no more than 800 MB on my _VS-PM_ storage.

The default value for the configuration file is `/etc/vredenslack/pm/config`. This file contains information such as where the storage is, repository configuration, where the database file is (this is kept in your storage folder too), etc.

The default value for the storage location is `/var/lib/vredenslack/pm`. If you initialize your _VS-PM_ and then reinitialize it with another storage location, the previous storage location is **NOT** removed. So keep that in mind.

Before you run `vs-pm` you should configure it if you are not happy with the default values. We still recommend you use the default value for the configuration file since this is a very small file and if you do not use the default location then you are doomed to add the configuration file parameter each time you use `vs-pm`.

    vs-pm config init \
      --config-file /etc/vredenslack/pm/config \
      --base-folder /var/lib/vredenslack/pm

You can check your configuration settings using

    vs-pm [OPTIONS] config show

# GENERIC OPTIONS

There are a few options which typically affect all `vs-pm` commands:

## --slackware-version &lt;version>

This configuration overrides the automatic slackware version detection `vs-pm` does each time it is invoked. This is useful specially after new releases of Slackware for which there are still no releases of some packages and you are sure that the version used in a previous Slackware version will compile and work.

## --configuration-file &lt;path to file>

This setting, as mentioned in the CONFIGURATION section, will use the given file as configuration file. Make sure this is a valid configuration file. If you use a non-default configuration file make sure you always add this option or create an alias

    alias vs-pm='vs-pm --configuration file /path/to/file'

## --quiet

This reduces verbosity level.

## --help

This shows the extended help.

# PACKAGE MANAGEMENT

Package management is all that pertains to actually installing packages.

## vs-pm update

Before installing anything you should update the packages for your slackware version. Running this will update all your repositories and will scan them for any changes.

## vs-pm rescan

If you do manual changes to some slackbuilds this will come in handy. It lets you rescan your current copies of repositories and update the internal package database. This step is always run when you do an update, but sometimes you do not want to fetch changes from the remote repositories.

## vs-pm list \[&lt;search term>\]

List all packages, optionally specify a search term to filter the list. The search term matches any sub-string of each package. For example, the 'breof' term matches 'libreoffice'.

## vs-pm info &lt;package name>

Print out info on a package. Use `vs-pm list` to find a package name and try it out!

## vs-pm \[OPTIONS\] install &lt;package name> \[&lt;package name> ...\]

Install one or more packages. This command has special options which allows you to control dependencies and what packages are installed. Running this command without options will do 3 actions on the packages specified and all their dependencies. The 3 actions are: downloading all necessary sources to build the package, build the package and install the package.

### --env &lt;key>=&lt;value>

This option lets you define environment variables and their values which will be carried on to the build process. You can provide this option multiple times. For example, you can use `--env COMPAT32=yes` which is a well known variable used for multilib Slackwares.

### --only-deps

This option only runs the actions on the dependencies. Note that this means **ALL** dependencies not just the immediate dependencies of the packages you typed.

### --nodeps

This option only runs the actions on the packages you provided. Note that this can mean that the packages will not build. So keep that in mind.

### --noinstall

This prevents the action _install_ to be executed for all packages.

### --nobuild

This prevents the action _build_ to be executed for all packages.

### --nodownload

This prevents the action _download_ to be executed for all packages.

## vs-pm \[OPTIONS\] ignore &lt;package name> \[&lt;reason>\]

Ignore a certain package. Use this to prevent the package manager from installing certain packages. A good example is the jdk package from the official SlackBuilds.org for which the download has to be done manually. You can also provide an optional message. This message will be printed on the command line (except if --quiet was specified) if the package manager tries to install the ignored package.

## vs-pm \[OPTIONS\] unignore &lt;package name>

Remove a package from the ignore list.

# REPOSITORIES

Starting in version 0.4, Vredeslack Package Manager supports multiple repositories. Now you can add Rsync, URL, Git and local filesystem repositories.

## Supported repository addresses

The repository type depends on the address used. Here is a list of supported address syntax and the corresponding repository type

        rsync server: rsync://example.org/folder/
        git repository: git@github.com:user/project.git
        local folder: /path/to/folder

## vs-pm \[OPTIONS\] repo add &lt;name> &lt;address> &lt;version> \[&lt;priority>\]

This command lets you add your own repository. Note that when running _VS-PM_ for the first time you will get the official SlackBuilds.org repositories configured for you. You can, however, add your own repositories and even remove the ones from the official SlackBuilds.org. Repositories are scanned in the order they are added.

The `name` parameter is any name you would like to give your repository. It must be unique. The `address` parameter is the actual URL for the repository. See the ["Supported repository addresses"](#supported-repository-addresses) for more info on how to setup one.

The `version` parameter is required and lets _VS-PM_ know for which slackware version the repository is for. You can specify `all` which means that the repository is valid for all versions.

The `priority` parameter is optional, defaults to 1, and sets the priority this repository has over others. Priority is used when two repositories contain a package with the same name. The package from the repository with the highest priority value is chosen over the other.

## vs-pm \[OPTIONS\] repo remove &lt;name>

Removes a repository from the list.

## vs-pm \[OPTIONS\] repo priority &lt;name> &lt;priority>

Set a new value for a repository's priority. This command will automatically trigger a rescan of all repositories unless the option `--no-auto-rescan` is passed.

## vs-pm \[OPTIONS\] repo list

Lists all repositories which are used to search for packages for your slackware version only. Remember that you can override the autodetection of the slackware version using the `--slackware-version` option.

# AUTHOR

JB Ribeiro (Vredens) - &lt;vredens@gmail.com>

# COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in [http://dev.perl.org/licenses/](http://dev.perl.org/licenses/).
